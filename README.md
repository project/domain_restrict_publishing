Domain Restrict Publishing
==========================

## Summary

Domain Restrict Publishing is a helper module building on top of the excellent
[Domain Access](https://www.drupal.org/project/domain) module.

The module enables site administrators to restrict the domains a user can
publish to, based on their assigned domains.

The module was designed to work with the "Set domain access status for all
content" and allows content to be promoted to other domains while keeping
control over which domain editors can edit it.


## Installation

* Install as usual, see http://drupal.org/node/70151 for further information.


## Configuration

* Configure user permissions in Administration » People » Permissions:

  - Set unrestricted domain access status for all content

    This permission skips the module alterations and reverts to the default
    Domain Access behavior.

## Contact

### Current maintainers:

* Dan Braghis (zerolab) - https://www.drupal.org/user/354424

### This project has been sponsored by:

* Torchbox
  We work with people who make the world a better place.
